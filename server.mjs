import express from 'express'
const port = 8085

console.log('Process versions: ',process.versions)
console.log('time', new Date())
console.log('запускаем сервер обмена данными.');

const SERVER = express()
SERVER.use(express.static( './public'))
SERVER.listen(port)
console.log(`Запуск приложения\n - http://localhost:${port}`);
