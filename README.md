# Flow-builder (prototype)

## Dependencies setup
```
npm install
```

### Start app
```
npm run start
```

### Change port into server.mjs file (if need)

### Open app in the browser (check the port)
[localhost:8085](http://localhost:8085)
