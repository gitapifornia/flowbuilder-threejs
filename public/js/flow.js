/*

*/

import {

    Vector3,
    Mesh,
    Color,

    MeshBasicMaterial,
    BufferGeometry,
    Float32BufferAttribute,
    PointsMaterial,
    Points,
    DoubleSide,
    CanvasTexture,
    BufferAttribute,

    Raycaster,
    WebGLRenderer,
    Scene,

    OrthographicCamera,

} from '../lib/three.module.js'

import {
    LineGeometry
} from '../lib/LineGeometry.js'

import {
    LineMaterial
} from '../lib/LineMaterial.js'
import {
    Line2
} from '../lib/Line2.js'


export const MOUSE_BTN_LEFT = 1
export const MOUSE_BTN_RIGHT = 2
export const MOUSE_CLICK = 4
export const KEY_SHIFT = 8

const MOUSE_BTN = [MOUSE_BTN_LEFT, 0, MOUSE_BTN_RIGHT]

const matrix_tree = new Uint8Array(16).fill(0)


const titles = []
const title_txt = []
const title_ctx = []
const title_width = 256
const title_height = 32
let titles_used = 0
const titles_txt_h = 2048

const minBlockWidth = 0.15
const minBlockHeight = 0.05
const blockTextHeight = 0.018
const blockTextSpace = 0.005
const blockBottomHeight = 0.01
const blockPointsOffset = 0.008
const blockPointSize = 12
const blockPointSizeR = 0.008

const lineWidth = 0.004

const colorsCollection = [
    "#63b598", "#ce7d78", "#ea9e70", "#a48a9e", "#c6e1e8", "#648177", "#0d5ac1",
    "#f205e6", "#1c0365", "#14a9ad", "#4ca2f9", "#a4e43f", "#d298e2", "#6119d0",
    "#d2737d", "#c0a43c", "#f2510e", "#651be6", "#79806e", "#61da5e", "#cd2f00",
    "#9348af", "#01ac53", "#c5a4fb", "#996635", "#b11573", "#4bb473", "#75d89e",
    "#2f3f94", "#2f7b99", "#da967d", "#34891f", "#b0d87b", "#ca4751", "#7e50a8",
    "#c4d647", "#e0eeb8", "#11dec1", "#289812", "#566ca0", "#ffdbe1", "#2f1179",
    "#935b6d", "#916988", "#513d98", "#aead3a", "#9e6d71", "#4b5bdc", "#0cd36d",
    "#250662", "#cb5bea", "#228916", "#ac3e1b", "#df514a", "#539397", "#880977",
    "#f697c1", "#ba96ce", "#679c9d", "#c6c42c", "#5d2c52", "#48b41b", "#e1cf3b",
    "#5be4f0", "#57c4d8", "#a4d17a", "#225b8", "#be608b", "#96b00c", "#088baf",
    "#f158bf", "#e145ba", "#ee91e3", "#05d371", "#5426e0", "#4834d0", "#802234",
    "#6749e8", "#0971f0", "#8fb413", "#b2b4f0", "#c3c89d", "#c9a941", "#41d158",
    "#fb21a3", "#51aed9", "#5bb32d", "#807fb", "#21538e", "#89d534", "#d36647",
    "#7fb411", "#0023b8", "#3b8c2a", "#986b53", "#f50422", "#983f7a", "#ea24a3",
    "#79352c", "#521250", "#c79ed2", "#d6dd92", "#e33e52", "#b2be57", "#fa06ec",
    "#1bb699", "#6b2e5f", "#64820f", "#1c271", "#21538e", "#89d534", "#d36647",
    "#7fb411", "#0023b8", "#3b8c2a", "#986b53", "#f50422", "#983f7a", "#ea24a3",
    "#79352c", "#521250", "#c79ed2", "#d6dd92", "#e33e52", "#b2be57", "#fa06ec",
    "#1bb699", "#6b2e5f", "#64820f", "#1c271", "#9cb64a", "#996c48", "#9ab9b7",
    "#06e052", "#e3a481", "#0eb621", "#fc458e", "#b2db15", "#aa226d", "#792ed8",
    "#73872a", "#520d3a", "#cefcb8", "#a5b3d9", "#7d1d85", "#c4fd57", "#f1ae16",
    "#8fe22a", "#ef6e3c", "#243eeb", "#1dc18", "#dd93fd", "#3f8473", "#e7dbce",
    "#421f79", "#7a3d93", "#635f6d", "#93f2d7", "#9b5c2a", "#15b9ee", "#0f5997",
    "#409188", "#911e20", "#1350ce", "#10e5b1", "#fff4d7", "#cb2582", "#ce00be",
    "#32d5d6", "#17232", "#608572", "#c79bc2", "#00f87c", "#77772a", "#6995ba",
    "#fc6b57", "#f07815", "#8fd883", "#060e27", "#96e591", "#21d52e", "#d00043",
    "#b47162", "#1ec227", "#4f0f6f", "#1d1d58", "#947002", "#bde052", "#e08c56",
    "#28fcfd", "#bb09b", "#36486a", "#d02e29", "#1ae6db", "#3e464c", "#a84a8f",
    "#911e7e", "#3f16d9", "#0f525f", "#ac7c0a", "#b4c086", "#c9d730", "#30cc49",
    "#3d6751", "#fb4c03", "#640fc1", "#62c03e", "#d3493a", "#88aa0b", "#406df9",
    "#615af0", "#4be47", "#2a3434", "#4a543f", "#79bca0", "#a8b8d4", "#00efd4",
    "#7ad236", "#7260d8", "#1deaa7", "#06f43a", "#823c59", "#e3d94c", "#dc1c06",
    "#f53b2a", "#b46238", "#2dfff6", "#a82b89", "#1a8011", "#436a9f", "#1a806a",
    "#4cf09d", "#c188a2", "#67eb4b", "#b308d3", "#fc7e41", "#af3101", "#ff065",
    "#71b1f4", "#a2f8a5", "#e23dd0", "#d3486d", "#00f7f9", "#474893", "#3cec35",
    "#1c65cb", "#5d1d0c", "#2d7d2a", "#ff3420", "#5cdd87", "#a259a4", "#e4ac44",
    "#1bede6", "#8798a4", "#d7790f", "#b2c24f", "#de73c2", "#d70a9c", "#25b67",
    "#88e9b8", "#c2b0e2", "#86e98f", "#ae90e2", "#1a806b", "#436a9e", "#0ec0ff",
    "#f812b3", "#b17fc9", "#8d6c2f", "#d3277a", "#2ca1ae", "#9685eb", "#8a96c6",
    "#dba2e6", "#76fc1b", "#608fa4", "#20f6ba", "#07d7f6", "#dce77a", "#77ecca"]

const hexToRgb = (hex) => {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}


const prepare_titles = () => {

    console.log('text texture')

    let canvas = document.createElement('CANVAS')
    canvas.width = title_width
    canvas.height = titles_txt_h
    let ctx = canvas.getContext('2d')

    let t = new CanvasTexture(
        canvas,
        //UVMapping,
        //ClampToEdgeWrapping,
        //ClampToEdgeWrapping,
        //LinearMipmapLinearFilter,
        //LinearMipmapLinearFilter,
        //RGBAFormat,
        //1
    )
    //t.generateMipmaps = false

    let txt_n = title_txt.length
    title_txt.push(t)
    title_ctx.push(ctx)

    ctx.font = '600 ' + title_height + "px arial"

    let x = 0

    let count = Math.trunc(canvas.height / title_height)
    console.log(count)
    for (let i = 0; i < count; i++) {
        //ctx.fillText('Hello world '+title_height, x, y*title_height)
        let uv = i * title_height / canvas.height
        titles.push([i * title_height + title_height, uv, txt_n])
    }

    //document.body.appendChild(canvas)

}

const makeText = (text, align) => {
    if (titles_used === titles.length) {
        prepare_titles()
    }
    let t = titles[titles_used]
    titles_used = titles_used + 1
    //
    let y = t[0]
    let uv = t[1]
    let txt_n = t[2]
    let ctx = title_ctx[txt_n]
    ctx.fillStyle = 'rgba(255,255,255,0)'
    ctx.fillRect(0, y, y + title_height, title_width)
    ctx.fillStyle = '#ffffff'
    ctx.textBaseline = 'middle'
    if (align === 0) {
        ctx.textAlign = 'left'
        ctx.fillText(text, 0, y - title_height / 2)
    } else {
        ctx.textAlign = 'right'
        ctx.fillText(text, title_width, y - title_height / 2)
    }
    //
    let uv_step = title_height / titles_txt_h
    const pass_geometry = new BufferGeometry()

    let hw = (minBlockWidth - 0.01) / 2
    let hh = blockTextHeight / 2
    pass_geometry.setAttribute('position', new BufferAttribute(new Float32Array([
        -hw, -hh, 0,
        -hw, hh, 0,
        hw, -hh, 0,
        hw, -hh, 0,
        -hw, hh, 0,
        hw, hh, 0
    ]), 3))
    const eps = 0.00
    pass_geometry.setAttribute('uv', new BufferAttribute(new Float32Array([
        0, 1.0 - (uv + uv_step + eps),
        0, 1.0 - (uv + eps),
        1, 1.0 - (uv + uv_step + eps),
        1, 1.0 - (uv + uv_step + eps),
        0, 1.0 - (uv + eps),
        1, 1.0 - (uv + eps)
    ]), 2))

    let m = new Mesh(
        pass_geometry,
        new MeshBasicMaterial({
            map: title_txt[txt_n],
            side: DoubleSide,
            transparent: true,
            depthTest: false,
            depthWrite: false,
            //wireframe   : true,
        })
    )

    //m.scale.set( 0.2, 0.01, 1.0)

    return m
}


const drawInlineSVG = (ctx, rawSVG, callback) => {

    var svg = new Blob([rawSVG], { type: "image/svg+xml;charset=utf-8" }),
        domURL = self.URL || self.webkitURL || self,
        url = domURL.createObjectURL(svg),
        img = new Image;

    img.onload = function () {
        ctx.drawImage(this, 0, 0);
        domURL.revokeObjectURL(url);
        callback(this);
    };

    img.src = url;
}

const roundRect = (ctx, x, y, width, height, radius, strokeWidth, strokeColor, fillColor) => {
    ctx.strokeStyle = strokeColor
    ctx.fillStyle = fillColor
    ctx.lineWidth = strokeWidth
    ctx.beginPath()
    ctx.moveTo(x + radius, y)
    ctx.lineTo(x + width - radius, y)
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius)
    ctx.lineTo(x + width, y + height - radius)
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height)
    ctx.lineTo(x + radius, y + height)
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius)
    ctx.lineTo(x, y + radius)
    ctx.quadraticCurveTo(x, y, x + radius, y)
    ctx.closePath();
    ctx.fill()
    ctx.stroke()
}


export class Flow {
    constructor(canvas) {
        this.canvas = canvas
        this.blocks = []          // список всех блоков
        this.ids = 1           // инкрементор id блоков
        this.objects = new Map()   // блоки по id

        this.renderer = null
        this.camera = null
        this.scene = null
        this.raycaster = new Raycaster()
        this._v3 = new Vector3()
        this._p = [0, 0, 0]


        this.cameraScale = 1      // зум камеры
        this.cameraScaleTo = 1
        this.cameraPosTo = [0, 0]  // положение камеры

        this.frameTime = 0

        this.mouseBtnActive = 0
        this.mouseWheelDelta = 0
        this.mouseX = 0
        this.mouseY = 0
        this.mouseOx = 0
        this.mouseOy = 0
        this._lastOx = 0
        this._lastOy = 0


        this.lineData = null
        this.lineIsActive = false
        this.overIsTitle = false
        this.overObj = null
        this.overIsPoint = 0         // 0-none 1-out 2-in
        this.overIsPointN = 0
        this.lineFromObj = null
        this.lineFromPoint = 0
        this.lineFromPointN = 0


        this.pointsMaterial = new PointsMaterial({
            vertexColors: true,
            side: DoubleSide,
            transparent: false,
            depthTest: false,
            depthWrite: false,
            //wireframe   : true,
            size: blockPointSize,
        })
        this.pointsMaterial.sizeAttenuation = false


        this._line = null
        this.lineIsActive = false

        this._bg_texture = null

        // обрабатываем события

        canvas.addEventListener('mousedown', e => {
            this.mouseBtnActive = this.mouseBtnActive | MOUSE_BTN[e.button]
        }, false)

        canvas.addEventListener('mouseup', e => {
            this.mouseBtnActive = this.mouseBtnActive & (~MOUSE_BTN[e.button])
        }, false)

        canvas.addEventListener('mousewheel', e => {
            this.mouseWheelDelta = this.mouseWheelDelta + e.deltaY
            e.stopPropagation()
        }, false)

        canvas.addEventListener('click', e => {
            this.mouseBtnActive = this.mouseBtnActive | MOUSE_CLICK
        }, false)

        canvas.addEventListener('contextmenu', e => {
            e.preventDefault()
        })

        canvas.addEventListener('mousemove', e => {
            let x = e.layerX / canvas.width
            let y = e.layerY / canvas.height
            this.mouseX = e.layerX
            this.mouseY = e.layerY
            this.mouseOx = x * 2 - 1
            this.mouseOy = -y * 2 + 1
        }, false)

        this._prepareLine()
        this._createBlockBg()
    }

    // добавляет блок
    addBlock(pos) {

        const block = Object.seal({
            id: this.ids,
            //p   : [Math.random()*2-1,Math.random()*2-1],
            p: pos,
            b: [0, 0, 0, 0],
            in: [
                {
                    t: 'test',
                    c: 0,
                },
                {
                    t: 'увход 1',
                    c: 0,
                },
                {
                    t: 'упроверка',
                    c: 0,
                },
                {
                    t: 'test',
                    c: 0,
                },
                {
                    t: 'увход 1',
                    c: 0,
                },
                {
                    t: 'упроверка',
                    c: 0,
                }
            ],
            out: [
                {
                    t: 'test',
                    c: 0,
                    n: 0,
                    m: null,
                    l: null,
                },
                {
                    t: 'test',
                    c: 0,
                    n: 0,
                    m: null,
                    l: null,
                },
                {
                    t: 'test',
                    c: 0,
                    n: 0,
                    m: null,
                    l: null,
                },
                {
                    t: 'test',
                    c: 0,
                    n: 0,
                    m: null,
                    l: null,
                }
            ],
            m: null,
            pm: null,
        })

        this.ids = this.ids + 1

        this.objects.set(block.id, block)

        block.b[0] = minBlockWidth
        block.b[1] = minBlockHeight + (block.in.length + block.out.length) * (blockTextHeight + blockTextSpace) + blockBottomHeight
        block.b[2] = block.b[0] / 2
        block.b[3] = block.b[1] / 2

        let hw = block.b[2]
        let hh = block.b[3]

        let geometry = new BufferGeometry()
        geometry.setAttribute('position', new BufferAttribute(new Float32Array([
            -hw, -hh, 0,
            -hw, hh, 0,
            hw, -hh, 0,
            hw, -hh, 0,
            -hw, hh, 0,
            hw, hh, 0
        ]), 3))
        geometry.setAttribute('uv', new BufferAttribute(new Float32Array([
            0, 0,
            0, 1,
            1, 0,
            1, 0,
            0, 1,
            1, 1
        ]), 2))

        let m = new Mesh(
            geometry,
            new MeshBasicMaterial({
                map: this._bg_texture,
                //color: 0xffffff,
                side: DoubleSide,
                transparent: true,
                depthTest: false,
                depthWrite: false,
                //wireframe   : true,
            })
        )
        m.position.set(block.p[0], block.p[1], 0)

        block.m = m


        let x = hw + blockPointsOffset
        let y = hh - minBlockHeight

        let position = []
        let colors = []
        for (let i = 0; i < block.out.length; i++) {
            block.out[i].m = makeText(block.out[i].t, 1)
            position.push(x, y, 0)
            let c = hexToRgb(colorsCollection[i])
            colors.push(c.r / 255, c.g / 255, c.b / 255)
            y = y - blockTextHeight - blockTextSpace
        }
        x = -hw - blockPointsOffset
        for (let i = 0; i < block.in.length; i++) {
            block.in[i].m = makeText(block.in[i].t, 0)
            position.push(x, y, 0)
            let c = hexToRgb(colorsCollection[10 + i])
            colors.push(c.r / 255, c.g / 255, c.b / 255)
            y = y - blockTextHeight - blockTextSpace
        }

        geometry = new BufferGeometry()
        geometry.setAttribute('position', new Float32BufferAttribute(position, 3))
        geometry.setAttribute('color', new Float32BufferAttribute(colors, 3))


        let pm = new Points(
            geometry,
            this.pointsMaterial,
        )
        pm.position.set(block.p[0], block.p[1], 0)
        block.pm = pm


        this.blocks.push(block)
    }

    // подготовка движка отрисовки
    _prepareRender() {
        // Подготовка рендера
        const r = new WebGLRenderer({
            canvas: this.canvas,
            antialias: true,
            alpha: false,
            //stencil   : false,
            precision: 'lowp',
            //powerPreference: 'high-performance', // глючит на некоторых десктоп устройствах
            //premultipliedAlpha   : true,
            //preserveDrawingBuffer: true,
            /*logarithmicDepthBuffer:true,*/
        })

        r.setPixelRatio(window.devicePixelRatio)

        r.autoClear = false
        r.shadowMap.enabled = false

        this.renderer = r
        this.camera = new OrthographicCamera(-1, 1, 1, -1, 0, 1)
        this.camera.position.z = 1

        this.scene = new Scene()
        this.scene.background = new Color(0x606060)

        //window.addEventListener('resize', renderResize)

        this.frameTime = performance.now()
        this.animframeID = 0

        this._renderResize()


        this.render()
    }

    // обновляем пропорции и размер вьюпорта
    _renderResize() {
        const w = this.canvas.width
        const h = this.canvas.height

        const aspect = w / h

        this.renderer.setSize(w, h)

        this.camera.aspect = aspect;
        this.camera.left = -this.cameraScale * aspect / 2
        this.camera.right = this.cameraScale * aspect / 2
        this.camera.top = this.cameraScale / 2
        this.camera.bottom = -this.cameraScale / 2
        this.camera.updateProjectionMatrix()
    }

    // создает подложку блока
    _createBlockBg() {
        let canvas = document.createElement('CANVAS')
        canvas.width = 512
        canvas.height = 512
        let ctx = canvas.getContext('2d')
        this._bg_texture = new CanvasTexture(canvas)

        let xml = `<svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" stroke="null">
            <g id="a">
                <title>a</title>
                <rect stroke-width="4" rx="14" id="svg_7" height="508" width="508" y="2" x="1.50003" stroke="#000" fill="#6D97AB"/>
                <path stroke-width="2" stroke="#000" id="svg_6" d="m1.50001,16c0,-7.60731 6.39269,-14 14,-14l480,0c7.60731,0 14,6.39269 14,14l0,17.5032l-508,0l0,-17.5032z" opacity="undefined" fill="#2D6862"/>
            </g>
        </svg>`

        drawInlineSVG(ctx, xml, () => {
            //setInterval(tick,100)

            this._prepareRender()
        })
    }

    // подготавливает материал линии
    _prepareLine() {
        const geometry = new LineGeometry()
        geometry.setPositions([0, 0, 0, 0, 0, 0])

        const material = new LineMaterial({
            color: 0xf0f050,
            linewidth: 0.003, // in pixels
            dashed: false,
            //alphaToCoverage: true,
        })

        this._line = new Line2(geometry, material)
    }

    // проверяет положение курсора на какой объект и под объекты сейчас наведен
    checkOver() {
        if (!this.overIsActive) {
            this.overObj = null
            this.overIsTitle = false
            this.overIsPoint = 0

            this.mouseToWorld(this.mouseOx, this.mouseOy)
            let x = this._v3.x
            let y = this._v3.y
            let i = this.blocks.length
            while (i--) {
                let b = this.blocks[i]
                let l = b.p[0] - b.b[2] - blockPointsOffset - blockPointSizeR
                let r = b.p[0] + b.b[2] + blockPointsOffset + blockPointSizeR
                let t = b.p[1] + b.b[3]
                let d = b.p[1] - b.b[3]

                if (x > l && x < r && y < t && y > d) {
                    this.overObj = b
                    break
                }
            }

            if (this.overObj) {
                let l = this.overObj.p[0] - this.overObj.b[2]
                let r = this.overObj.p[0] + this.overObj.b[2]
                let t = this.overObj.p[1] + this.overObj.b[3]
                let d = this.overObj.p[1] - this.overObj.b[3]
                if (x > l && x < r && y >= (t - minBlockHeight / 2)) {
                    this.overIsTitle = true
                } else {
                    // out
                    if (x > r) {
                        let py = t - minBlockHeight
                        let px = r + blockPointsOffset
                        for (let i = 0; i < this.overObj.out.length; i++) {
                            let dx = Math.abs(x - px)
                            let dy = Math.abs(y - py)
                            if (dx < blockPointSizeR && dy < blockPointSizeR) {
                                this.overIsPoint = 1
                                this.overPointN = i
                            }
                            py = py - blockTextHeight - blockTextSpace
                        }
                    }
                    // in
                    if (x < l) {
                        let py = t - minBlockHeight - (blockTextHeight + blockTextSpace) * this.overObj.out.length
                        let px = l - blockPointsOffset
                        for (let i = 0; i < this.overObj.in.length; i++) {
                            let dx = Math.abs(x - px)
                            let dy = Math.abs(y - py)
                            if (dx < blockPointSizeR && dy < blockPointSizeR) {
                                this.overIsPoint = 2
                                this.overPointN = i
                            }
                            py = py - blockTextHeight - blockTextSpace
                        }
                    }
                }
            }
        }
    }

    // подготавливается к отрисовки кадра
    renderBefore(delta) {
        this.checkOver()
        this.control()

        this.cameraScale = this.cameraScale + (this.cameraScaleTo - this.cameraScale) * 0.2

        this.camera.left = -this.cameraScale * this.camera.aspect / 2
        this.camera.right = this.cameraScale * this.camera.aspect / 2
        this.camera.top = this.cameraScale / 2
        this.camera.bottom = -this.cameraScale / 2
        this.camera.updateProjectionMatrix()

        let p = this.camera.position
        p.x = p.x + (this.cameraPosTo[0] - p.x) * 1.0
        p.y = p.y + (this.cameraPosTo[1] - p.y) * 1.0

        this.pointsMaterial.size = blockPointSize / this.cameraScale

    }

    // отрисовка объектов
    renderAfter(delta) {

        this._line.material.linewidth = lineWidth / this.cameraScale

        for (let i = 0; i < this.blocks.length; i++) {
            const block = this.blocks[i]

            block.m.position.x = block.p[0]
            block.m.position.y = block.p[1]

            this.updateMatrix(block.m)
            this.renderMesh(block.m)


            let x = block.m.position.x
            let y = block.m.position.y + block.b[3] - minBlockHeight
            for (let j = 0; j < block.out.length; j++) {
                let a = block.out[j]
                a.m.position.x = x
                a.m.position.y = y
                this.updateMatrix(a.m)
                this.renderMesh(a.m)
                y = y - blockTextHeight - blockTextSpace
                //
                if (a.l !== null) {
                    this._line.geometry = a.l
                    this.renderMesh(this._line)
                }
            }

            for (let j = 0; j < block.in.length; j++) {
                let a = block.in[j]
                a.m.position.x = x
                a.m.position.y = y
                this.updateMatrix(a.m)
                this.renderMesh(a.m)
                y = y - blockTextHeight - blockTextSpace
            }

            //
            block.pm.position.x = block.p[0]
            block.pm.position.y = block.p[1]

            this.updateMatrix(block.pm)
            this.renderMesh(block.pm)

        }

        if (this.lineIsActive) {
            if (this.lineData !== null) {
                this._line.geometry = this.lineData
                //this.update_matrix(this._line)
                this.renderMesh(this._line)
            }
        }
    }

    // цикл отрисовки
    render() {
        const d = performance.now()
        const delta = d - this.frameTime
        //
        this.frameTime = d

        this.renderBefore(delta)

        this.renderer.setRenderTarget(null)
        this.renderer.clear(false, true, true)

        this.renderer.abd_render_start(this.scene, this.camera, null)

        this.renderAfter(delta)

        this.renderer.abd_render_end()

        this.animframeID = requestAnimationFrame(() => this.render())
    }

    // обновляет матрицу объекта
    updateMatrix(m) {
        m.matrixWorld.compose(m.position, m.quaternion, m.scale)

        let t = 0
        let p = 0
        let l = m.children
        do {
            while (p !== l.length) {

                const o = l[p]

                o.matrix.compose(o.position, o.quaternion, o.scale)
                o.matrixWorld.multiplyMatrices(m.matrixWorld, o.matrix)

                p = p + 1
                if (o.children.length !== 0 && t < matrix_tree.length) {
                    matrix_tree[t] = p
                    m = o
                    l = o.children
                    p = 0
                    t = t + 1
                }

            }

            if (t === 0) {
                break
            }
            m = m.parent
            l = m.children
            t = t - 1
            p = matrix_tree[t]

        } while (true)
    }

    // отрисовка объекта
    renderMesh(m) {

        if (m.isMesh || m.isSkinnedMesh || m.isPoints) {
            if (this.renderer.abd_frustum(m)) {
                const p = this.renderer.abd_prepareObject(m, this.scene, this.camera, m.material)
                this.renderer.abd_renderObject(m, m.geometry, m.material, p)
            }
        }
        let t = 0
        let p = 0
        let l = m.children
        do {
            while (p !== l.length) {

                const o = l[p]
                if (o.isMesh || o.isSkinnedMesh || m.isPoints) {
                    if (this.renderer.abd_frustum(o)) {
                        const p = this.renderer.abd_prepareObject(o, this.scene, this.camera, o.material)
                        this.renderer.abd_renderObject(o, o.geometry, o.material, p)
                    }
                }

                p = p + 1
                if (o.children.length !== 0 && t < matrix_tree.length) {
                    matrix_tree[t] = p
                    m = o
                    l = o.children
                    p = 0
                    t = t + 1
                }

            }

            if (t === 0) {
                break
            }
            m = m.parent
            l = m.children
            t = t - 1
            p = matrix_tree[t]

        } while (true)
    }

    // операции с объеками, перетаскивание, соединение, передвижение камеры
    control() {
        // управление камерой роликом мышки
        if (this.mouseWheelDelta !== 0) {
            this.cameraScaleTo = this.cameraScaleTo + this.mouseWheelDelta * 0.002
            this.cameraScaleTo = Math.min(Math.max(this.cameraScaleTo, 0.5), 10)
            this.mouseWheelDelta = 0
        }

        if (this.mouseBtnActive & MOUSE_BTN_LEFT) {
            let dx = this._lastOx - this.mouseOx
            let dy = this._lastOy - this.mouseOy

            if (!this.lineIsActive) {
                if (this.overIsTitle) {
                    this.overIsActive = true
                    this.overObj.p[0] = this.overObj.p[0] - dx * this.camera.right
                    this.overObj.p[1] = this.overObj.p[1] - dy * this.camera.top
                    this.refreshLines(this.overObj)
                } else {
                    if (this.overIsPoint !== 0) {
                        this.lineIsActive = true
                        this.lineFromObj = this.overObj
                        this.lineFromPoint = this.overIsPoint
                        this.lineFromPointN = this.overPointN
                        if (this.lineFromPoint === 1) {
                            this.disconnectOut(this.lineFromObj, this.lineFromPointN)
                        } else {
                            this.disconnectIn(this.lineFromObj, this.lineFromPointN)
                        }
                    } else {
                        this.cameraPosTo[0] = this.cameraPosTo[0] + dx * this.camera.right
                        this.cameraPosTo[1] = this.cameraPosTo[1] + dy * this.camera.top
                    }
                }
            } else {

                if (this.lineFromPoint === 1) {
                    this.getOutPosition(this.lineFromObj, this.lineFromPointN)
                } else {
                    this.getInPosition(this.lineFromObj, this.lineFromPointN)
                }
                let x = this._p[0]
                let y = this._p[1]

                if (this.lineData !== null) {
                    this.lineData.dispose()
                }

                if (this.overObj !== null && this.overObj !== this.lineFromObj && this.overIsPoint !== 0 && this.overIsPoint !== this.lineFromPoint) {
                    if (this.overIsPoint === 1) {
                        this.getOutPosition(this.overObj, this.overPointN)
                        this.lineData = this.makeCurve(this._p[0], this._p[1], x, y)
                    } else {
                        this.getInPosition(this.overObj, this.overPointN)
                        this.lineData = this.makeCurve(x, y, this._p[0], this._p[1])
                    }
                } else {
                    this.mouseToWorld(this.mouseOx, this.mouseOy)
                    if (this.lineFromPoint === 1) {
                        this.lineData = this.makeCurve(x, y, this._v3.x, this._v3.y)
                    } else {
                        this.lineData = this.makeCurve(this._v3.x, this._v3.y, x, y)
                    }
                }
            }

        } else {
            if (this.lineIsActive) {
                if (this.overObj !== null && this.overObj !== this.lineFromObj && this.overIsPoint !== 0 && this.overIsPoint !== this.lineFromPoint) {
                    if (this.lineFromPoint === 1) {
                        this.connect(this.lineFromObj, this.lineFromPointN, this.overObj, this.overPointN)
                    } else {
                        this.connect(this.overObj, this.overPointN, this.lineFromObj, this.lineFromPointN)
                    }
                }
            }

            if (this.lineData !== null) {
                this.lineData.dispose()
                this.lineData = null
            }

            this.overIsActive = false
            this.lineIsActive = false
        }

        this._lastOx = this.mouseOx
        this._lastOy = this.mouseOy
    }

    // переносит координаты курсора в сцену
    mouseToWorld(x, y) {
        this._v3.x = x
        this._v3.y = y
        this._v3.z = 0.0
        this.raycaster.setFromCamera(this._v3, this.camera)
        this._v3.x = this.raycaster.ray.origin.x
        this._v3.y = this.raycaster.ray.origin.y
        this._v3.z = this.raycaster.ray.origin.z
    }

    // координаты точки выхода
    getOutPosition = (b, n) => {
        this._p[0] = b.p[0] + b.b[2] + blockPointsOffset
        this._p[1] = b.p[1] + b.b[3] - minBlockHeight - (blockTextHeight + blockTextSpace) * n
        return this._p
    }

    // координаты точки входа
    getInPosition = (b, n) => {
        this._p[0] = b.p[0] - b.b[2] - blockPointsOffset

        this._p[1] = b.p[1] + b.b[3] - minBlockHeight - (blockTextHeight + blockTextSpace) * b.out.length
        this._p[1] = this._p[1] - (blockTextHeight + blockTextSpace) * n
        return this._p
    }

    // выстраивает кривую линию
    makeCurve(sx, sy, ex, ey) {
        let p = []

        let dx = Math.max(Math.abs(ex - sx), 0.1)

        let a = [sx + dx * 0.5, sy]
        let b = [ex - dx * 0.5, ey]

        let steps = 50

        p.push(sx, sy, 0)
        for (let i = 1; i < steps; i++) {
            let t = i / steps
            let x1 = sx + (a[0] - sx) * t
            let y1 = sy + (a[1] - sy) * t
            let x2 = a[0] + (b[0] - a[0]) * t
            let y2 = a[1] + (b[1] - a[1]) * t
            let x3 = b[0] + (ex - b[0]) * t
            let y3 = b[1] + (ey - b[1]) * t

            let x4 = x1 + (x2 - x1) * t
            let y4 = y1 + (y2 - y1) * t
            let x5 = x2 + (x3 - x2) * t
            let y5 = y2 + (y3 - y2) * t

            let x6 = x4 + (x5 - x4) * t
            let y6 = y4 + (y5 - y4) * t

            p.push(x6, y6, 0)
        }
        p.push(ex, ey, 0)

        const geometry = new LineGeometry()
        geometry.setPositions(p)
        return geometry
    }

    // создает линии из точки входа и выхода объекта
    makeLines(b) {
        let sx = 0
        let sy = 0
        for (let i = 0; i < b.out.length; i++) {
            let a = b.out[i]
            if (a.l !== null) {
                a.l.dispose()
                a.l = null
            }
            if (a.c !== 0) {
                this.getOutPosition(b, i)
                sx = this._p[0]
                sy = this._p[1]
                let o = this.objects.get(a.c)
                this.getInPosition(o, a.n)

                a.l = this.makeCurve(sx, sy, this._p[0], this._p[1])
            }
        }
    }

    // обновляет линии
    refreshLines(b) {
        this.makeLines(b)
        for (let i = 0; i < b.in.length; i++) {
            let a = b.in[i]
            if (a.c !== 0) {
                this.makeLines(this.objects.get(a.c))
            }
        }
    }

    // создает соединение
    connect(a, an, b, bn) {
        let id = b.in[bn].c
        if (id !== 0) {
            let c = this.objects.get(id)
            for (let i = 0; i < c.out.length; i++) {
                let cp = c.out[i]
                if (cp.c === b.id && cp.n === bn) {
                    cp.c = 0
                    cp.n = 0
                    if (cp.l !== null) {
                        cp.l.dispose()
                        cp.l = null
                    }
                }
            }
        }
        b.in[bn].c = a.id

        a.out[an].c = b.id
        a.out[an].n = bn

        this.makeLines(a)
    }

    // обрывает соединение
    disconnectOut(a, n) {
        let id = a.out[n].c
        if (id !== 0) {
            let c = this.objects.get(id)
            let b = c.in[a.out[n].n]
            if (b.c === a.id) {
                b.c = 0
            }
        }

        if (a.out[n].l !== null) {
            a.out[n].l.dispose()
        }
        a.out[n].c = 0
        a.out[n].l = null
        a.out[n].n = 0
    }

    // обрывает соединение
    disconnectIn(a, n) {
        let id = a.in[n].c
        if (id !== 0) {
            let c = this.objects.get(id)
            for (let i = 0; i < c.out.length; i++) {
                let b = c.out[i]
                if (b.c === a.id && b.n === n) {
                    if (b.l !== null) {
                        b.l.dispose()
                    }
                    b.c = 0
                    b.n = 0
                    b.l = null
                }
            }
        }
        a.in[n].c = 0
    }


}
